<?php

namespace App\twig;

use NumberFormatter;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MyTwigExtension extends AbstractExtension
{

    public function getFilters() {
        return [
          new TwigFilter('price', [$this, 'price'])
        ];
    }

    public function getFunctions() {
        return [
          new TwigFunction('randomBeatle', [$this, 'randomBeatle'])
        ];
    }

    public function price(int $price): string {
        $fmt = new NumberFormatter( 'fr_FR', NumberFormatter::CURRENCY );
        return $fmt->formatCurrency($price, "EUR");
    }

    public function randomBeatle(bool $withFamilyName = true): string {
        $beatles = [
            ["Paul", "McCartney"],
            ["John", "Lennon"],
            ["Ringo", "Starr"],
            ["George", "Harrison"]
        ];

        $selected = rand(0, count($beatles) - 1);
//        return $beatles[$selected][0];
        return $beatles[$selected][0] . ($withFamilyName ? ' ' .$beatles[$selected][1] : '');
    }


}
