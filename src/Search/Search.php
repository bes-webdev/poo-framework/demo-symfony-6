<?php

namespace App\Search;

use Doctrine\Common\Collections\ArrayCollection;

class Search
{

    private ?string $searchText = null;
    private ArrayCollection $categories;
    private int $maxPrice;

    /**
     * @return string
     */
    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    /**
     * @param string $searchText
     */
    public function setSearchText(string $searchText): void
    {
        $this->searchText = $searchText;
    }

    /**
     * @return array
     */
    public function getCategories(): ArrayCollection
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories(ArrayCollection $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return int
     */
    public function getMaxPrice(): int
    {
        return $this->maxPrice;
    }

    /**
     * @param int $maxPrice
     */
    public function setMaxPrice(int $maxPrice): void
    {
        $this->maxPrice = $maxPrice;
    }






}
