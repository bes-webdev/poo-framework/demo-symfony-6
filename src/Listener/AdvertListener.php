<?php

namespace App\Listener;

use App\Entity\Advert;
use App\twig\MyTwigExtension;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class AdvertListener
{


    public function __construct()
    {
    }

    public function preUpdate(Advert $advert, LifecycleEventArgs $args) {
        $advert->setUpdateDate(new \DateTime());
    }

}
