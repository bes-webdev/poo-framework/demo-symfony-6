<?php

namespace App\Repository;

use App\Entity\Advert;
use App\Search\Search;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Advert>
 *
 * @method Advert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advert[]    findAll()
 * @method Advert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advert::class);
    }

    public function add(Advert $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Advert $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findWithCategory($limit = 10): array
    {
        $qb = $this->createQueryBuilder('a');

        $qb->setMaxResults($limit)
            ->orderBy('a.id', 'DESC')
            ->leftJoin('a.category', 'category')
            ->addSelect('category');

        return $qb->getQuery()->getResult();

    }

    //    /**
    //     * @return Advert[] Returns an array of Advert objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

//    public function findOneBySomeField($value): ?Advert
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    /**
     * @return Advert[]
     */
    public function findBySearch(Search $search): array
    {
        $qb = $this->createQueryBuilder('a');
        if ($search->getSearchText()) {
            $qb->where('a.title like :text')
                ->setParameter('text', '%' .$search->getSearchText(). '%');

        }
        if ($search->getCategories() && $search->getCategories()->count()) {
            $qb->leftJoin('a.category', 'category')
                ->addSelect('category')
                ->andWhere('category in(:categories)')
                ->setParameter('categories', $search->getCategories())
            ;
        }
        if ($search->getMaxPrice()) {
            $qb->andWhere('a.price<=:price')
                ->setParameter('price', $search->getMaxPrice());
        }
        return $qb->getQuery()->getResult();

    }
}
