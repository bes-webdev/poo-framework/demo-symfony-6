<?php

namespace App\Controller;

use App\Form\MyAccountType;
use App\Form\NewPasswordType;
use App\Repository\AdvertRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    #[Route('/profil', name: 'app_profile')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(MyAccountType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('app_profile');
        }
        return $this->render('profile/index.html.twig', ['form' => $form->createView()]);
    }

    #[Route('/profil/mes-annonces', name: 'app_profile_adverts')]
    public function myAdverts(AdvertRepository $advertRepository): Response
    {
        // Récupérer l'utilisateur connecté
        $user = $this->getUser();
        $adverts = $advertRepository->findBy(['owner' => $user]);
        return $this->render('profile/adverts.html.twig', ['adverts' => $adverts]);
    }

    #[Route('/profil/changer-mot-de-passe', name: 'app_profile_new_password')]
    public function newPassword(EntityManagerInterface $em, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        // Récupérer l'utilisateur connecté
        $user = $this->getUser();
        $form = $this->createForm(NewPasswordType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $user->getPlainPassword()
            );
            $user->setPassword($hashedPassword);

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('app_profile');
        }
        return $this->render('profile/newPassword.html.twig', ['form' => $form->createView()]);
    }
}
