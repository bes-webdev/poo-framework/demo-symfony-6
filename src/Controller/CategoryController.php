<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{

    #[Route('/category/create/{name}', name: 'app_create_category')]
    public function createCategory($name, EntityManagerInterface $entityManager): Response {
        $category = new Category();
        $category->setName($name);

        $entityManager->persist($category);
        $entityManager->flush();

        return new Response('category created '.$category->getId());
    }

    #[Route('/category/delete/{id}', name: 'app_delete_category')]
    public function deleteCategory($id, EntityManagerInterface $entityManager, CategoryRepository $categoryRepository): Response {
        $category = $categoryRepository->find($id);

        if ($category) {
            $entityManager->remove($category);
            $entityManager->flush();

        }

        return new Response('category delete '.$id);
    }

}
