<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Form\SearchType;
use App\Repository\AdvertRepository;
use App\Repository\CategoryRepository;
use App\Search\Search;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(AdvertRepository $advertRepository): Response
    {
        $adverts = $advertRepository->findWithCategory();
        return $this->render('default/home.html.twig', ['annonces' => $adverts]);
    }

    #[Route('/a/{id<\d+>}', name: 'viewAdvert')]
    public function viewAdvert(int $id, AdvertRepository $advertRepository): Response
    {
        $advert = $advertRepository->find($id);
       return $this->render('default/advert.html.twig', ['advert' => $advert]);
    }

    #[Route('/ad/{id<\d+>}', name: 'viewAdvert2')]
    public function viewAdvert2(Advert $advert): Response
    {
       return $this->render('default/advert.html.twig', ['advert' => $advert]);
    }

    #[Route('/category/{name<[a-zA-Z-]+>}', name: 'category')]
    public function category(string $name, CategoryRepository $categoryRepository, AdvertRepository $advertRepository): Response
    {
        $category = $categoryRepository->findOneBy(['name' => $name]);
        $adverts = $advertRepository->findBy(['category' => $category]);

        return $this->render('default/category.html.twig', ['name' => $name, 'adverts' => $adverts]);

    }

    #[Route('/search', name: 'search')]
    public function search(Request $request, AdvertRepository $advertRepository): Response
    {
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);
        $form->handleRequest($request);
        $adverts = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $adverts = $advertRepository->findBySearch($search);
        }
        return $this->render('default/search.html.twig', ['searchForm' => $form->createView(), 'results' => $adverts]);
    }


}
