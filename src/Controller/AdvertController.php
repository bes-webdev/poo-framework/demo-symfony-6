<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\Photo;
use App\Form\AdvertType;
use App\Form\PhotoType;
use App\Repository\AdvertRepository;
use App\Repository\CategoryRepository;
use App\twig\MyTwigExtension;
use App\Upload\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvertController extends AbstractController
{

    #[Route('/advert/create', name: 'create_advert')]
    #[IsGranted('ROLE_USER')]
    public function createAdvert(Request $request, FileUploader $fileUploader, MyTwigExtension $myTwigExtension, AdvertRepository $advertRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response {
       $advert = new Advert();
       $form = $this->createForm(AdvertType::class, $advert);

       $form->handleRequest($request);
       if ($form->isSubmitted()) {
           if ($form->isValid()) {
               foreach ($form->get('gallery')->get('photos') as $photoForm ) {
                   $fileUploader->uploadFilesFromForm($photoForm);
                   foreach ($advert->getGallery()->getPhotos() as $photo) {
                       $entityManager->persist($photo);
                   }
               }

               $advert->setOwner($this->getUser());
               $entityManager->persist($advert);
               $entityManager->flush();
               return $this->redirectToRoute('viewAdvert', ['id'=> $advert->getId()]);
           }
       }
       return $this->render('default/create-advert.html.twig', ['advertForm' => $form->createView()]);
//
//
    }

    #[Route('/advert/update/{id}', name: 'update_advert')]
    #[IsGranted('ROLE_USER')]
    public function updateAvert(Advert $advert, Request $request, FileUploader $fileUploader, EntityManagerInterface $entityManager): Response {
        if ($this->getUser() !== $advert->getOwner()) {
            throw new AccessDeniedException('Not your advert');
        }
        $form = $this->createForm(AdvertType::class, $advert);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                foreach ($form->get('gallery')->get('photos') as $photoForm ) {
                    $fileUploader->uploadFilesFromForm($photoForm);
                    foreach ($advert->getGallery()->getPhotos() as $photo) {
                        $photo->setGallery($advert->getGallery());
                        $entityManager->persist($photo);
                    }
                }

                $entityManager->persist($advert);
                $entityManager->flush();
                return $this->redirectToRoute('viewAdvert', ['id'=> $advert->getId()]);
            }
        }

        return $this->render('default/create-advert.html.twig', ['advertForm' => $form->createView()]);

    }

    #[Route('/photo/create', name: 'create_photo')]
    public function createPhoto(Request $request, FileUploader $fileUploader, EntityManagerInterface $entityManager): Response {

        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $fileUploader->uploadFilesFromForm($form);
            $entityManager->persist($form->getData());
            $entityManager->flush();
        }

        return $this->render('default/create-photo.html.twig', ['photoForm' => $form->createView()]);
    }

}
